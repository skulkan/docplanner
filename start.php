<?php

require __DIR__ . '/vendor/autoload.php';

$router = new \jblond\router\Router();
$router->setBasepath('');
$router->init();

$router->get('/health', function() { echo 'I\'m alive ;)'; });


$router->get('/slots/date_from/(.*)/date_to/(.*)/sort_type/(.*)', function($from, $to, $sort) {
    $dateFrom = DateTimeImmutable::createFromFormat('Y-m-d\TH:i:sT', $from);
    if (!$dateFrom) {
        echo 'Wrong date_from format';
        return;
    }

    $dateTo = DateTimeImmutable::createFromFormat('Y-m-d\TH:i:sT', $to);
    if (!$dateTo) {
        echo 'Wrong date_to format';
        return;
    }

    try {
        $sortType = (new \App\Search\Sort\SlotsSortMapper())->getSortingInstanceByName($sort);
    } catch (\App\Search\Sort\InvalidSlotsSortName $exception) {
        echo 'Wrong sort_type';
        return;
    }

    $slotsFinder = new \App\Search\SlotsFinder(\App\Factory\EntityManagerFactory::getInstance());
    $result = $slotsFinder->searchByTimeAndSort($dateFrom, $dateTo, $sortType);

    header("Content-Type: application/json");
    echo json_encode($result);
});

$router->run();