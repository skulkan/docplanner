<?php

use App\Import\DoctorsApi\Config;
use App\Import\DoctorsApi\DoctorsFetcher;
use App\Import\Exception\FetchError;
use App\Import\ImportedDoctorInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * I would unit test one class, but I will try to fully test its logic
 */
final class DoctorsFetcherTest extends TestCase
{
    /**
     * @dataProvider invalidHttpCodesProvider
     */
    public function testNot200CodeResponse(int $code): void
    {
        $this->expectException(FetchError::class);

        $response = $this->createMock(ResponseInterface::class);
        $response->expects($this->once())
            ->method('getStatusCode')
            ->willReturn($code);

        $client = $this->createMock(Client::class);
        $client->expects($this->once())
            ->method('get')
            ->willReturn($response);

        $doctorsFetcher = new DoctorsFetcher($client, $this->getConfigMock());
        $doctorsFetcher->fetchDoctors();
    }

    public function testClientException(): void
    {
        $this->expectException(FetchError::class);

        $exception = $this->createMock(GuzzleException::class);

        $client = $this->createMock(Client::class);
        $client->expects($this->once())
            ->method('get')
            ->willThrowException($exception);

        $doctorsFetcher = new DoctorsFetcher($client, $this->getConfigMock());
        $doctorsFetcher->fetchDoctors();
    }

    public function testProperEmptyResponse(): void
    {
        $stream = $this->createMock(StreamInterface::class);
        $stream->expects($this->once())
            ->method('getContents')
            ->willReturn('[]');

        $response = $this->createMock(ResponseInterface::class);
        $response->expects($this->once())
            ->method('getStatusCode')
            ->willReturn(200);
        $response->expects($this->once())
            ->method('getBody')
            ->willReturn($stream);

        $client = $this->createMock(Client::class);
        $client->expects($this->once())
            ->method('get')
            ->willReturn($response);

        $doctorsFetcher = new DoctorsFetcher($client, $this->getConfigMock());
        $doctors = $doctorsFetcher->fetchDoctors();

        $this->assertEquals([], $doctors);
    }

    public function testProperOneDoctorResponse()
    {
        $stream = $this->createMock(StreamInterface::class);
        $stream->expects($this->once())
            ->method('getContents')
            ->willReturn('[{"id":0,"name":"Adoring Shtern"}]');

        $response = $this->createMock(ResponseInterface::class);
        $response->expects($this->once())
            ->method('getStatusCode')
            ->willReturn(200);
        $response->expects($this->once())
            ->method('getBody')
            ->willReturn($stream);

        $client = $this->createMock(Client::class);
        $client->expects($this->once())
            ->method('get')
            ->willReturn($response);

        $doctorsFetcher = new DoctorsFetcher($client, $this->getConfigMock());
        $doctors = $doctorsFetcher->fetchDoctors();

        $this->assertCount(1, $doctors);

        $this->assertInstanceOf(
            ImportedDoctorInterface::class,
            $doctors[0]
        );

        $this->assertEquals(
            0,
            $doctors[0]->getId()
        );

        $this->assertEquals(
            'Adoring Shtern',
            $doctors[0]->getName()
        );
    }

    private function getConfigMock(): Config
    {
        $config = $this->createMock(Config::class);

        $config->expects($this->once())
            ->method('getHost')
            ->willReturn('http://example.com');
        $config->expects($this->once())
            ->method('getAuthUser')
            ->willReturn('user');
        $config->expects($this->once())
            ->method('getAuthPass')
            ->willReturn('pass');

        return $config;
    }

    public function invalidHttpCodesProvider(): array
    {
        return [
            [100],
            [101],
            [201],
            [204],
            [400],
            [404],
            [500]
        ];
    }
}
