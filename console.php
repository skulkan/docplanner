<?php

require __DIR__.'/vendor/autoload.php';

use App\Command\ImportSlotsCommand;
use App\Import\DoctorsApi\Config;
use App\Import\DoctorsApi\DoctorsFetcher;
use App\Import\DoctorsApi\SlotsFetcher;
use GuzzleHttp\Client;
use Symfony\Component\Console\Application;

$application = new Application();

// Could be improved by moving creation into DI and factory
$application->add(new ImportSlotsCommand(
    new DoctorsFetcher(
        new Client(),
        new Config()
    ),
    new SlotsFetcher(
        new Client(),
        new Config()
    ),
    new \App\Persistence\DoctorsPersistence(\App\Factory\EntityManagerFactory::getInstance()),
    new \App\Persistence\SlotsPersistence(\App\Factory\EntityManagerFactory::getInstance()),
));

$application->run();