<?php

namespace App\Search\Sort;

use Doctrine\ORM\QueryBuilder;

interface SlotsSorter
{
    public function extendQueryBySorting(QueryBuilder $queryBuilder);
}