<?php

namespace App\Search\Sort;

use Doctrine\ORM\QueryBuilder;

class SlotsSorterByDuration implements SlotsSorter
{
    public function extendQueryBySorting(QueryBuilder $queryBuilder)
    {
        $queryBuilder->orderBy('s.end - s.start', 'ASC');
    }
}