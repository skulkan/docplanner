<?php

namespace App\Search\Sort;

class SlotsSortMapper
{
    private array $map = [
        'duration' => SlotsSorterByDuration::class,
        'datetime' => SlotsSorterByDateAndTime::class
    ];

    /**
     * @throws InvalidSlotsSortName
     */
    public function getSortingInstanceByName(string $name): SlotsSorter
    {
        if (isset($this->map[$name])) {
            return new ($this->map[$name])();
        }

        throw new InvalidSlotsSortName();
    }
}