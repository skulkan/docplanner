<?php

namespace App\Search\Sort;

use Doctrine\ORM\QueryBuilder;

class SlotsSorterByDateAndTime implements SlotsSorter
{
    public function extendQueryBySorting(QueryBuilder $queryBuilder)
    {
        $queryBuilder->orderBy('s.start', 'ASC');
    }
}