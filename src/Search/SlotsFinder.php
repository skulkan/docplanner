<?php

namespace App\Search;

use App\Entity\Slot;
use App\Search\Sort\SlotsSorter;
use Doctrine\ORM\EntityManagerInterface;

class SlotsFinder
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function searchByTimeAndSort(
        \DateTimeImmutable $dateFrom,
        \DateTimeImmutable $dateTo,
        SlotsSorter $sorter
    ) : array
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('s');
        $queryBuilder->from(Slot::class, 's');
        $queryBuilder->where('s.start >= :dateFrom');
        $queryBuilder->andWhere('s.start < :dateTo');
        $queryBuilder->setParameter('dateFrom', $dateFrom);
        $queryBuilder->setParameter('dateTo', $dateTo);

        $sorter->extendQueryBySorting($queryBuilder);

        return $queryBuilder->getQuery()->getResult();
    }
}