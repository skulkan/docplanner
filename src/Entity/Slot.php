<?php

namespace App\Entity;

use App\Import\ImportedDoctorInterface;
use App\Import\ImportedSlotInterface;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="slots")
 */
class Slot implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="Doctor")
     * @ORM\JoinColumn(name="doctor_id", referencedColumnName="id")
     */
    private Doctor $doctor;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $start;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $end;

    public function __construct(Doctor $doctor, DateTimeImmutable $start, DateTimeImmutable $end)
    {
        $this->doctor = $doctor;
        $this->start = $start;
        $this->end = $end;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getDoctor(): Doctor
    {
        return $this->doctor;
    }

    public function getStart(): DateTimeImmutable
    {
        return $this->start;
    }

    public function getEnd(): DateTimeImmutable
    {
        return $this->end;
    }

    public static function createForDoctorFromImportedSlot(
        Doctor $doctor,
        ImportedSlotInterface $importedSlot
    ): Slot
    {
        return new Slot($doctor, $importedSlot->getStart(), $importedSlot->getEnd());
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'start' => $this->getStart()->format('c'),
            'end' => $this->getEnd()->format('c'),
            'doctor' => $this->getDoctor()
        ];
    }
}