<?php

namespace App\Import\DoctorsApi;

use App\Import\DoctorsApi\DTO\ImportedDoctor;
use App\Import\Exception\FetchError;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class DoctorsFetcher
{
    private const URI = '/api/doctors';

    private Client $client;
    private Config $config;

    public function __construct(Client $client, Config $config)
    {
        $this->client = $client;
        $this->config = $config;
    }

    /**
     * @throws FetchError Any of throws should have easy to identify exception (for example different exceptions),
     *                    I will use only one with different messages
     */
    public function fetchDoctors(): array
    {
        try {
            $result = $this->client->get(
                sprintf($this->config->getHost() . self::URI),
                [
                    'auth' => [
                        $this->config->getAuthUser(),
                        $this->config->getAuthPass()
                    ]
                ]
            );

            if ($result->getStatusCode() !== 200) {
                throw new FetchError('Doctor fetch fails.');
            }

        } catch (GuzzleException $exception) {
            throw new FetchError(sprintf('Guzzle exception: %s', $exception->getMessage()));
        }

        // Some kind of collection will be more readable here and on interface
        $output = [];
        foreach (json_decode($result->getBody()->getContents(), true) as $row) {
            $output[] = new ImportedDoctor($row['id'], $row['name']);
        }

        return $output;
    }

}