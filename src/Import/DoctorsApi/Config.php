<?php

namespace App\Import\DoctorsApi;

class Config
{
    private string $host;
    private string $authUser;
    private string $authPass;

    // This should be injected, I will use default values
    function __construct(
        string $host = 'https://cryptic-cove-05648.herokuapp.com',
        string $authUser = 'docplanner',
        string $authPass = 'docplanner'
    ) {
        $this->host = $host;
        $this->authUser = $authUser;
        $this->authPass = $authPass;
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function getAuthUser(): string
    {
        return $this->authUser;
    }

    public function getAuthPass(): string
    {
        return $this->authPass;
    }


}