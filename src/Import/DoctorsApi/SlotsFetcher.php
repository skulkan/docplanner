<?php

namespace App\Import\DoctorsApi;

use App\Entity\Doctor;
use App\Import\DoctorsApi\DTO\ImportedSlot;
use App\Import\Exception\FetchError;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class SlotsFetcher
{
    private const URI = '/api/doctors/%d/slots';

    private Client $client;
    private Config $config;

    public function __construct(Client $client, Config $config)
    {
        $this->client = $client;
        $this->config = $config;
    }

    /**
     * @throws FetchError Any of throws should have easy to identify exception (for example different exceptions),
     *                    I will use only one with different messages
     */
    public function fetchSlotsForDoctor(Doctor $doctor): array
    {
        try {
            $result = $this->client->get(
                sprintf($this->config->getHost() . sprintf(self::URI, $doctor->getId())),
                [
                    'auth' => [
                        $this->config->getAuthUser(),
                        $this->config->getAuthPass()
                    ]
                ]
            );

            if ($result->getStatusCode() !== 200) {
                throw new FetchError('Slots fetch fails.');
            }

        } catch (GuzzleException $exception) {
            throw new FetchError(sprintf('Guzzle exception: %s', $exception->getMessage()));
        }

        // Some kind of collection will be more readable here and on interface
        $output = [];
        foreach (json_decode($result->getBody()->getContents(), true) as $row) {
            $output[] = new ImportedSlot(
                new \DateTimeImmutable($row['start']),
                new \DateTimeImmutable($row['end'])
            );
        }

        return $output;
    }
}