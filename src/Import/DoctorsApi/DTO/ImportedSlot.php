<?php

namespace App\Import\DoctorsApi\DTO;

use App\Import\ImportedSlotInterface;
use DateTimeImmutable;

class ImportedSlot implements ImportedSlotInterface
{
    private DateTimeImmutable $start;
    private DateTimeImmutable $end;

    public function __construct(DateTimeImmutable $start, DateTimeImmutable $end)
    {
        $this->start = $start;
        $this->end = $end;
    }

    public function getStart(): DateTimeImmutable
    {
        return $this->start;
    }

    public function getEnd(): DateTimeImmutable
    {
        return $this->end;
    }
}