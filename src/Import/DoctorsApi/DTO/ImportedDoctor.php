<?php

namespace App\Import\DoctorsApi\DTO;

use App\Import\ImportedDoctorInterface;

class ImportedDoctor implements ImportedDoctorInterface
{
    private int $id;
    private string $name;

    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }
}