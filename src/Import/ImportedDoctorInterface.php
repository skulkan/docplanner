<?php

namespace App\Import;

interface ImportedDoctorInterface
{
    public function getId(): int;
    public function getName(): string;
}