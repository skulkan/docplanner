<?php

namespace App\Import;

use DateTimeImmutable;

interface ImportedSlotInterface
{
    public function getStart(): DateTimeImmutable;
    public function getEnd(): DateTimeImmutable;
}