<?php

namespace App\Command;

use App\Entity\Doctor;
use App\Entity\Slot;
use App\Import\DoctorsApi\DoctorsFetcher;
use App\Import\DoctorsApi\SlotsFetcher;
use App\Import\Exception\FetchError;
use App\Persistence\DoctorsPersistence;
use App\Persistence\SlotsPersistence;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Possible improvements:
 *
 * Could be improved by splitting into two commands one for doctor and one for slots, where slot can be run with
 * doctor id as argument. It will allow fetching slots for all doctor in one time by many threads.
 *
 * Could handle transaction to import all or nothing
 *
 * If doctor already exists I skip it, could do update of not existing slots
 */
class ImportSlotsCommand extends Command
{
    protected static $defaultName = 'app:import:slots';

    private DoctorsFetcher $doctorFetcher;
    private SlotsFetcher $slotsFetcher;
    private DoctorsPersistence $doctorsRepository;
    private SlotsPersistence $slotsRepository;

    public function __construct(
        DoctorsFetcher     $doctorFetcher,
        SlotsFetcher       $slotsFetcher,
        DoctorsPersistence $doctorsRepository,
        SlotsPersistence $slotsRepository
    )
    {
        parent::__construct(self::$defaultName);
        $this->doctorFetcher = $doctorFetcher;
        $this->slotsFetcher = $slotsFetcher;
        $this->doctorsRepository = $doctorsRepository;
        $this->slotsRepository = $slotsRepository;
    }

    protected function configure(): void
    {
        $this->setHelp('This command do import of slots and doctors from external api');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln(sprintf('Start import doctors at <info>%s</info>.', (new \DateTime())->format('c')));

        try {
            $doctors = $this->doctorFetcher->fetchDoctors();
        } catch (FetchError $exception) {
            $output->writeln(sprintf('Doctors fetch fails with error: %s', $exception->getMessage()));
            return Command::FAILURE;
        }

        $output->writeln(sprintf('Start import slots for <info>%d</info> doctors.', count($doctors)));

        foreach ($doctors as $importedDoctor) {
            $doctor = Doctor::createFromImportedDoctor($importedDoctor);
            try {
                $this->doctorsRepository->saveDoctor($doctor);
            } catch (UniqueConstraintViolationException $exception) {
                // Could be handled in better place
                $output->writeln(sprintf(
                    'Doctor <info>%s</info> with id <info>%d</info> already imported.',
                    $doctor->getName(),
                    $doctor->getId()
                ));

                // It can raise EntityManager is closed error, won't fix that but probably should reset it or reopen

                continue;
            }

            try {
                $this->importSlotsForDoctor($output, $doctor);
            } catch (FetchError $exception) {
                $output->writeln(sprintf('Slots fetch fails with error: %s', $exception->getMessage()));
                continue;
            }
        }

        return Command::SUCCESS;
    }

    /**
     * @throws FetchError
     */
    private function importSlotsForDoctor(OutputInterface $output, Doctor $doctor)
    {
        $output->writeln(sprintf(
            'Import slots for doctor <info>%s</info> with id <info>%d</info>.',
            $doctor->getName(),
            $doctor->getId()
        ));

        $slots = $this->slotsFetcher->fetchSlotsForDoctor($doctor);

        foreach ($slots as $importedSlot) {
            $slot = Slot::createForDoctorFromImportedSlot($doctor, $importedSlot);
            $this->slotsRepository->saveSlot($slot);
        }

        $output->writeln(sprintf(
            'Imported <info>%d</info> slots.',
            count($slots)
        ));
    }
}