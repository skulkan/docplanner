<?php

namespace App\Persistence;

use App\Entity\Doctor;

class DoctorsPersistence extends AbstractPersistence
{
    public function saveDoctor(Doctor $doctor): void
    {
        $this->getEntityManager()->persist($doctor);
        $this->getEntityManager()->flush();
    }
}