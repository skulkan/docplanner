<?php

namespace App\Persistence;

use App\Entity\Slot;

class SlotsPersistence extends AbstractPersistence
{
    public function saveSlot(Slot $slot): void
    {
        $this->getEntityManager()->persist($slot);
        $this->getEntityManager()->flush();
    }
}