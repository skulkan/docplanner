<?php

namespace App\Persistence;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Persistence objects could be split into Domain Interface and Infrastructure implementation of doctrine behavior
 */
abstract class AbstractPersistence
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }
}