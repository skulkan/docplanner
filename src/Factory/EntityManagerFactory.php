<?php

namespace App\Factory;

use Doctrine\ORM\EntityManagerInterface;

class EntityManagerFactory
{
    private static ?EntityManagerInterface $entityManager = null;

    public static function getInstance(): EntityManagerInterface
    {
        if (!self::$entityManager) {
            require_once "bootstrap.php";

            self::$entityManager = $entityManager;
        }

        return self::$entityManager;
    }
}