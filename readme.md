#Set up containers

`docker-compose up -d`

#Set up database

`docker-compose exec app vendor/bin/doctrine orm:schema-tool:create`

if database exists remove old one:

`docker-compose exec app vendor/bin/doctrine orm:schema-tool:drop --force`

#Run unit tests

`docker-compose exec app ./vendor/bin/phpunit tests`

#Run import command

`docker-compose exec app php console.php app:import:slots`

#Try api

If it's alive

`0.0.0.0:8000/health`

and do request like

`0.0.0.0:8000/slots/date_from/2020-02-01T15:00:00%2b00:00/date_to/2020-02-01T18:15:00%2b00:00/sort_type/datetime`

#Task description

DP Phone Backend Coding Challenge
The goal of the challenge is to pull available visits list (called slots) from an external
supplier, and then to create an API endpoint that returns the list in requested order.
Part I: The supplier
The slots supplier API:
Located at: http://cryptic-cove-05648.herokuapp.com/
Doctors List endpoint : /api/doctors
Slots endpoint: /api/doctors/{id}/slots
The endpoints are protected using basic auth and the following credentials:
username: docplanner
password: docplanner
Create a console app that will pull the slots of all doctors from the API and then will persist
them in the database.

Part II: Sorting
Create two classes that implement SlotsSorter interface and handle following types of
sorting:
sorting by slot duration - from longest to shortest
sorting by slot date and time - from closest available to latest available
Part III: The API

Create an endpoint that returns a list of slots. It should take the following parameters:
sort_type which determines the way of sorting
date_from and date_to which narrow the timeframe of the slots

Basing on the sort_type value pick one of the sorting algotithms from the previous part.
Keep in mind the flexibility of this solution, so it is easy to extend it in the future (e.g. by
adding new sorting types).

Our expectations
Use the framework you feel comfortable with or take our Symfony boilerplate: GitHub -
mlebkowski/slots-app-boilerplate
If you have any issues — ask. Checking how you communicate with the team is an
important part of the challenge. Feel free to talk about any topics that come to mind
The task is designed for 6-12 hours, so you can’t perfect everything and you have to
choose your battles
This is just a test challenge, but treat it more as a large-scale production application — show
your best skills, so we can know you better
Leave comments in places you see as candidates for future improvements
We’ll look at how you use the framework/library, readability, maintainability, performance,
adherence to best coding practices, automated tests, etc. If you skip some of those, we will
have the opportunity to talk about your decisions later. At the moment, our app would
benefit the most from:
More automated testing
Focusing on Domain Driven Design methods
Improving scalability
Good luck!